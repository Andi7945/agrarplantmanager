import 'package:agrar_plant_manager/page/soil_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:agrar_plant_manager/page/home_page.dart';
import 'package:agrar_plant_manager/provider/google_sign_in.dart';
import 'package:provider/provider.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final String title = 'MainPage';
  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => GoogleSignInProvider(),
        child: MaterialApp(
          debugShowCheckedModeBanner: true,
          title: title,
          //theme: ThemeData.dark().copyWith(accentColor: Colors.indigo),
          home: HomePage(),//FirebaseAuth.instance.currentUser != null ? SoilPage() : HomePage(),
        ),
      );
}
