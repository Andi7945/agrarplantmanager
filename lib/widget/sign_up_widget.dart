import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:agrar_plant_manager/provider/google_sign_in.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'dart:developer' as developer;

class SignUpWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) => Padding(
        padding: EdgeInsets.all(32),
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Spacer(),
            Lottie.asset("assets/lottie/plant1.json"),
            Spacer(),
            Align(
              //alignment: Alignment.center,
              child: Text(
                'Welcome to the AgrarPlantManager',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(height: 8),
            Spacer(),
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                'Everything you need to manage your field. From cultivation and division of labor to marketing.',
                style: TextStyle(fontSize: 16),
              ),
            ),
            Spacer(),
            SizedBox(height: 20),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: Colors.green,
                onPrimary: Colors.white,
                minimumSize: Size(double.infinity, 50),
              ),
              icon: FaIcon(FontAwesomeIcons.google, color: Colors.white),
              label: Text('Sign Up with Google'),
              onPressed: () {
                final provider =
                Provider.of<GoogleSignInProvider>(context, listen: false);
                provider.login();
              },
            ),
            SizedBox(height: 40),
            Spacer(),
          ],
        ),
      );
}
