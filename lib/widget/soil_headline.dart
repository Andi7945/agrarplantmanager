

import 'package:flutter/material.dart';

class SoilHeadline extends StatefulWidget {
  final String text;
  final double space;
  final Function() notifyParent;
  const SoilHeadline({Key? key, required String this.text,
    required double this.space, required this.notifyParent}) : super(key: key);



  @override
  _SoilHeadlineState createState() => _SoilHeadlineState();
}

class _SoilHeadlineState extends State<SoilHeadline> {

  refresh() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 18), //ToDO: warum kann ich hier nicht widget.space eintragen??????
      child: Text(
        widget.text,//MyApp.title,
        style: TextStyle(fontWeight: FontWeight.w500),
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(24),
        boxShadow: [
          BoxShadow(color: Color.fromRGBO(0, 0, 0, .25), blurRadius: 16),
        ],
      ),
    );
  }

}

