import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:timeline_tile/timeline_tile.dart';

class PanelWidget extends StatelessWidget {
  final ScrollController controller;
  final PanelController panelController;

  const PanelWidget({
    Key? key,
    required this.controller,
    required this.panelController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => ListView(
    padding: EdgeInsets.zero,
    controller: controller,
    children: <Widget>[
      SizedBox(height: 6),
      /*Center(
        child: Text(
          'Sunflowers - Field XYZ',
          style: TextStyle(fontWeight: FontWeight.normal, fontSize: 24),
        ),
      ),*/
      Container(
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[

            const TimeLine(days: '5', text: "sowing", date: "09.07.2021", icon: Icons.clean_hands, color: Colors.green, isFirst: true,),
            const TimeLine(days: '5', text: "fertilize",date: "10.07.2021", icon: Icons.api, color: Colors.red, ),
            const TimeLine(days: '5', text: "fight weeds",date: "15.07.2021", icon: Icons.cleaning_services, color: Colors.red, ),
            const TimeLine(days: '5', text: "harvest",date: "23.08.2021", icon: Icons.agriculture_outlined, color: Colors.orange, isLast: true,),

        ],
      ),
    ),


      SizedBox(height: 12),
      /*Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          buildIconWidget('Crop rotation', Icons.local_florist, Colors.green),
          buildIconWidget('Prepare', Icons.agriculture, Colors.red),
          buildIconWidget('Sell harvest', Icons.monetization_on_outlined, Colors.amber),
          buildIconWidget('Information', Icons.donut_small_outlined, Colors.blue),
        ],
      ),
      SizedBox(height: 36),*/
      buildAboutText(),
      SizedBox(height: 36),
      buildImages(),
      SizedBox(height: 24),
    ],
  );

  Widget buildDragHandle() => GestureDetector(
    child: Center(
      child: Container(
        width: 30,
        height: 5,
        decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.circular(12),
        ),
      ),
    ),
    onTap: togglePanel,
  );

  void togglePanel() => panelController.isPanelOpen
      ? panelController.close()
      : panelController.open();

  Widget buildIconWidget(String label, IconData icon, Color color) => Column(
    children: <Widget>[
      Container(
        padding: EdgeInsets.all(16),
        child: Icon(icon, color: Colors.white),
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.15), blurRadius: 8),
          ],
        ),
      ),
      SizedBox(height: 12.0),
      Text(label),
    ],
  );

  Widget buildImages() => Container(
    padding: EdgeInsets.symmetric(horizontal: 24),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        TextButton.icon(
          onPressed: () { },
          icon: const Icon(Icons.cloud_upload_outlined),
          style: TextButton.styleFrom(primary: Colors.black),
          label: const Text('Your Images from the field'),
        ),
        //SizedBox(height: 12),
        Container(
          height: 120,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 10,
            itemBuilder: (context, index) => Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 4),
              child: Image.network(
                'https://source.unsplash.com/random?sunflowers+field&sig=$index',
                height: 110,
                width: 150,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ],
    ),
  );

  Widget buildAboutText() => Container(
    padding: EdgeInsets.symmetric(horizontal: 24),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Notize',
          style: TextStyle(fontWeight: FontWeight.w600),
        ),
        SizedBox(height: 12),
        TextField(
          keyboardType: TextInputType.multiline,
          textInputAction: TextInputAction.done,
        ),
        /*TextEditingController(text: "Don't forget to call Tom for helping me sow..."),
            focusNode: FocusNode(onKey: ),
            style: TextStyle(fontWeight: FontWeight.w600),
            cursorColor: Colors.red,
            backgroundCursorColor: Colors.white,
        )*/
      ],
    ),
  );
}

class TimeLine extends StatelessWidget { //Timeline
  const TimeLine({
    Key? key,
    required this.text,
    required this.days,
    required this.color,
    required this.icon,
    this.date = "",
    this.isLast = false,
    this.isFirst = false,
    this.font = 'Shrikhand',
  }) : super(key: key);

  final String date;
  final bool isLast;
  final bool isFirst;
  final IconData icon;
  final Color color;
  final String days;
  final String text;
  final String font;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minHeight: 20),
      child: Center(
        child: TimelineTile(
          alignment: TimelineAlign.center,
          isFirst: isFirst,
          isLast: isLast,
          indicatorStyle: IndicatorStyle(
            width: 35,
            height: 30,
            padding: const EdgeInsets.symmetric(
              horizontal: 5,
            ),
            drawGap: true,
            color: Colors.black12,

            iconStyle: IconStyle(
              color: color,
              iconData: icon,
            ),
          ),
          startChild: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  text,
                  textAlign: TextAlign.right,
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
                SizedBox(height: 12),
                Text(date)
                /*TextEditingController(text: "Don't forget to call Tom for helping me sow..."),
            focusNode: FocusNode(onKey: ),
            style: TextStyle(fontWeight: FontWeight.w600),
            cursorColor: Colors.red,
            backgroundCursorColor: Colors.white,
        )*/
              ],
            ),
          ),
        ),
      ),
    );
  }
}


