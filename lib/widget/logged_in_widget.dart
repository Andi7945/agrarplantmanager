import 'package:agrar_plant_manager/page/plants_page.dart';
import 'package:agrar_plant_manager/page/soil_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:agrar_plant_manager/provider/google_sign_in.dart';
import 'package:provider/provider.dart';

class LoggedInWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final user = FirebaseAuth.instance.currentUser!;
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
        backgroundColor: Colors.green,
        actions: [
          TextButton(
            child: Text('Logout', style: TextStyle(color: Colors.black38),),
            onPressed: () {
              final provider =
                  Provider.of<GoogleSignInProvider>(context, listen: false);
              provider.logout();
            },
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        //alignment: Alignment.topLeft,
        child: Column(
          children: [
            Container(
              child: Row(
                  children: [
                    SizedBox(height: 32),
                    CircleAvatar(
                      radius: 40,
                      backgroundImage: NetworkImage(user.photoURL!),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      alignment: Alignment.topLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 5),
                          Text(
                            user.displayName!,
                            style: TextStyle(color: Colors.black38, fontSize: 14),
                          ),
                          SizedBox(height: 5),
                          Text(
                            user.email!,
                            style: TextStyle(color: Colors.black38, fontSize: 14),
                          ),
                          SizedBox(height: 5),
                          Text(
                            "12 fields stored",
                            style: TextStyle(color: Colors.black38, fontSize: 14),
                          ),
                        ],
                      ),
                    )
                  ]
                )
              ),
            Column(
              //crossAxisAlignment: CrossAxisAlignment.start,
              //mainAxisAlignment: MainAxisAlignment.end,
              children: [

                SizedBox(height: 20),
                ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.grey,
                    onPrimary: Colors.white,
                    minimumSize: Size(double.infinity, 50),
                  ),
                  icon: Icon(Icons.map_outlined),
                  label: Text('My Soil'),
                  onPressed: () {
                    Navigator.push(context, new MaterialPageRoute(
                        builder: (context) => SoilPage()
                    ));
                  },
                ),


                SizedBox(height: 20),
                ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.grey,
                    onPrimary: Colors.white,
                    minimumSize: Size(double.infinity, 50),
                  ),
                  icon: Icon(Icons.ac_unit),
                  label: Text('Plants'),
                  onPressed: () {
                    Navigator.push(context, new MaterialPageRoute(
                        builder: (context) => PlantPageState()
                    ));
                  },
                ),
                SizedBox(height: 20),
                ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.grey,
                    onPrimary: Colors.white,
                    minimumSize: Size(double.infinity, 50),
                  ),
                  icon: Icon(Icons.message_outlined),
                  label: Text('Chats'),
                  onPressed: () {
                  },
                ),
                SizedBox(height: 20),
                ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.grey,
                    onPrimary: Colors.white,
                    minimumSize: Size(double.infinity, 50),
                  ),
                  icon: Icon(Icons.agriculture_outlined),
                  label: Text('Work'),
                  onPressed: () {
                  },
                ),
                SizedBox(height: 20),
                ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.grey,
                    onPrimary: Colors.white,
                    minimumSize: Size(double.infinity, 50),
                  ),
                  icon: Icon(Icons.monetization_on_outlined),
                  label: Text('Marked'),
                  onPressed: () {
                  },
                ),

                SizedBox(height: 60),
                ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.green,
                    onPrimary: Colors.white,
                    minimumSize: Size(double.infinity, 50),
                  ),
                  icon: Icon(Icons.search),
                  label: Text('find the perfect crop rotation'),
                  onPressed: () {
                  },
                ),
              ],
            )
            ]
        ),
      ),
      
    );
  }
}
