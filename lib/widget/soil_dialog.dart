


import 'package:agrar_plant_manager/page/soil_page.dart';
import 'package:agrar_plant_manager/widget/soil_panel_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';


class SoilDialog extends StatelessWidget {
  final String fieldID;
  final bool isOwner;
  const SoilDialog({
    required this.fieldID,
    required this.isOwner,
    Key? key
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController myController = TextEditingController();
    const plantList = ['Wheat', 'Rye', 'Clover', 'Oat', 'Corn', 'Lupine', 'Barley', 'Sugar beet',
    'Potato', 'Rapeseed', 'Sunflower', 'Broad bean', 'Vetch', 'Soybean'];
    //ToDo: hier aus der Datenbak eine Liste aller hinerlegten Pflanzen implementeiren
    return
      isOwner ?
          //Wenn der Eigentümer
      Container(
          width: double.maxFinite,
          height: double.maxFinite,
          child: ListView(
            children: <Widget>[
              //TextFieldSearch(initialList: plantList, label: "Currently planted: " + field.plant, controller: myController),
              Text("Click on a Task to se everything about it."),
              TimeLine(days: '5', text: "sowing", date: "09.07.2021", icon: Icons.clean_hands, color: Colors.green, isFirst: true,),
              TimeLine(days: '5', text: "fertilize",date: "10.07.2021", icon: Icons.api, color: Colors.red, ),
              TimeLine(days: '5', text: "fight weeds",date: "15.07.2021", icon: Icons.cleaning_services, color: Colors.red, ),
              TimeLine(days: '5', text: "harvest",date: "23.08.2021", icon: Icons.agriculture_outlined, color: Colors.orange, isLast: true,),
              Container(
                margin: EdgeInsets.symmetric(vertical: 20.0),
                height: 100.0,
                child:
                ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    buildIconWidget('Crop rotation', Icons.local_florist, Colors.green),
                    const SizedBox(width: 8,),
                    buildIconWidget('Prepare', Icons.agriculture, Colors.red),
                    const SizedBox(width: 8,),
                    buildIconWidget('Sell harvest', Icons.monetization_on_outlined, Colors.amber),
                    const SizedBox(width: 8,),
                    buildIconWidget('Information', Icons.donut_small_outlined, Colors.blue),
                  ],
                ),
              ),
              TextButton(
            style: TextButton.styleFrom(
              padding: const EdgeInsets.all(16.0),
              //backgroundColor: Colors.green,
              primary: Colors.black,
              textStyle: const TextStyle(fontSize: 20),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('OK'),
          ),
        ],

      )) :
        // Wenn nicht
      Container(
        width: double.maxFinite,
        child: ListView(
        children: <Widget>[
        Text("Your tasks:"),
        TimeLine(days: '5', text: "sowing", date: "09.07.2021", icon: Icons.clean_hands, color: Colors.green, isFirst: true,),
        TimeLine(days: '5', text: "fertilize",date: "10.07.2021", icon: Icons.api, color: Colors.red, isLast: true, ),
        const SizedBox(height: 8,),
        TextButton(
          style: TextButton.styleFrom(
            padding: const EdgeInsets.all(16.0),
            //backgroundColor: Colors.green,
            primary: Colors.black,
            textStyle: const TextStyle(fontSize: 20),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text('OK'),
        ),
      ],
    )
      );
  }

  Widget buildIconWidget(String label, IconData icon, Color color) => Column(
    children: <Widget>[
      Container(
        padding: EdgeInsets.all(16),
        child: Icon(icon, color: Colors.white),
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.15), blurRadius: 8),
          ],
        ),
      ),
      SizedBox(height: 12.0),
      Text(label),
    ],
  );
}

