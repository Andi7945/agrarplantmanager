
import 'dart:async';
import 'dart:core';

import 'package:agrar_plant_manager/util/theme.dart';
import 'package:agrar_plant_manager/widget/soil_dialog.dart';
import 'package:agrar_plant_manager/widget/soil_headline.dart';
import 'package:agrar_plant_manager/widget/soil_panel_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'dart:developer' as developer;
final GlobalKey<_HeadlineState> keyHeadline = GlobalKey<_HeadlineState>();

class SoilPage extends StatefulWidget {
  @override
  _SoilPageState createState() => _SoilPageState();
}

class _SoilPageState extends State<SoilPage> {
  static const double fabHeightClosed = 116.0;

  double fabHeight = fabHeightClosed;
  int fieldPosition = 0;
  Completer<GoogleMapController> _controller = Completer();
  double _pinPillPosition = -100;
  List<Field> fields = [];
  List<String> fieldIDs = [];

  //final panelController = PanelController();


  @override
  void initState() {
    super.initState();

    fabHeight = fabHeightClosed;

  }

  @override
  Widget build(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser!;
    return Scaffold(
      body: _mirFealltKeinNameEin(context,user),//_mirFealltKeinNameEin(context, user),
    );
  }


  Widget _saveFBDataTest(BuildContext context, User user){
    String msg = "Läuft..";
    Future<void> addField() async {
      CollectionReference fieldRef = FirebaseFirestore.instance.collection('field');
      // Call the user's CollectionReference to add a new user
      Field field = Field(
          corners: [],
          notice: "fertilize - 19.07.2021",
          ownerID: user.uid,
          plant: "Gerste",
          startPosition: GeoPoint(48.063915, 10.294112),
          zoom: 14.3,
        tilt: 45.33,
          bearing: 192.03
      );
      field.addCorners(GeoPoint(48.0654, 10.296798));
      field.addCorners(GeoPoint(48.066, 10.294525));
      field.addCorners(GeoPoint(48.065771, 10.294284));
      field.addCorners(GeoPoint(48.065152, 10.295529));


      fieldRef.add(field.toJson())
          .then((value) => developer.log("Field Added", name: "AddSoil"))
          .catchError((error) => developer.log("Firebase Failed: $error", name: "AddSoil")
      );

    }
    addField();
    return Center(
      child: Text(msg),
    );
  }

 // Widget _getFirebaseData(BuildContext context, User user){
  //  final fieldRef = FirebaseFirestore.instance.collection('field').doc('asd')
  //  List<QueryDocumentSnapshot<Field>> field = await
  //}




  Widget _mirFealltKeinNameEin(BuildContext context, User user){
    final panelHeightOpen = MediaQuery.of(context).size.height * 0.8;
    final panelHeightClosed = MediaQuery.of(context).size.height * 0.15;
/*    Future<void> addField() async {
      //get Field Data
      final fieldRef = FirebaseFirestore.instance.collection('field')
          .where('userID', isEqualTo: user.uid)
          .snapshots()
          .transform(Utils.transformer(Field.fromJson));
    }*/
    return StreamBuilder(
      stream: FirebaseFirestore.instance.collection('field')
          //.where('ownerID', isEqualTo: user.uid)
          .snapshots(),
      builder: (_, AsyncSnapshot<QuerySnapshot> snapshot) {
        if(snapshot.connectionState == ConnectionState.waiting){
          return Center(
              child: Container(
                child: Column(
                  children: [
                    //CircularProgressIndicator(),
                    Text(snapshot.connectionState.toString())
                  ],
                ),
              ) 
              
          );
        } else if(snapshot.hasError) {
          return Center(
            child: Text(snapshot.error.toString()),
          );
        } else if(snapshot.hasData){
            final List<QueryDocumentSnapshot> fieldData = snapshot.data!.docs;
            fields = [];
            fieldIDs = [];
            for(int x = 0; x < fieldData.length; x++){
              fields.add(Field.fromJson(fieldData[x].data()));
              fieldIDs.add(fieldData[x].id);

            }
            Set<Marker> markers = Set<Marker>();
            Set<Polyline> polylineSet = Set<Polyline>();
            //final globalKey = GlobalKey<ScaffoldState>();
            for(int i = 0; i < fields.length; i++){
              final Marker marker = Marker(
                  markerId: MarkerId(fields[i].plant),
                  infoWindow: InfoWindow(title: fields[i].plant,snippet: fields[i].notice),
                  position: LatLng(fields[i].startPosition.latitude,fields[i].startPosition.longitude),
                  //draggable: true,
                onTap: () async {
                      await showDialog<void>(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text(
                                  fields[fieldPosition].plant,
                                //style: const TextStyle(fontSize: 30),
                              ),
                              content: SoilDialog(fieldID: fieldIDs[0], isOwner: fields[0].ownerID==user.uid,),

                            );

                          },
                      );
                    },
              );
              markers.add(marker);

              List<LatLng> cornersCon = [];
              for(int x = 0; x < fields[i].corners.length; x++) {
                cornersCon.add(LatLng(fields[i].corners[x].latitude, fields[i].corners[x].longitude));
              }
              if(fields[i].corners.length > 0)
                cornersCon.add(LatLng(fields[i].corners[0].latitude, fields[i].corners[0].longitude));

              final polyline = Polyline(
                polylineId: PolylineId(fields[i].plant + "_border"),
                points: cornersCon,
                color: Color.fromARGB(250, 255, 0, 0),
                width: 2,
              );
              polylineSet.add(polyline);

            }
            if(fields.length < 1){
              return Center(
                child: Text("kein Feld angelegt. \nUserID:" + user.uid +
                "\n" + snapshot.data.toString()),
              );
            }
          //https://www.youtube.com/watch?v=Zz5hMvgiWmY
          return(
            Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
                GoogleMap(
                  markers: markers,
                  polylines: polylineSet,
                  mapType: MapType.satellite,
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                  initialCameraPosition: CameraPosition(
                    target: LatLng(fields[fieldPosition].startPosition.latitude, fields[fieldPosition].startPosition.longitude),
                    zoom: fields[fieldPosition].zoom,
                  ),
                  //onMapCreated: onMapCreated,
                  myLocationEnabled: false,
                  //zoomGesturesEnabled: true,
                  //zoomControlsEnabled: true,
                ),/*
                SlidingUpPanel(
                  controller: panelController,
                  maxHeight: panelHeightOpen,
                  minHeight: panelHeightClosed,
                  parallaxEnabled: true,
                  parallaxOffset: .5,
                  body: GoogleMap(
                    markers: markers,
                    polylines: polylineSet,
                    mapType: MapType.satellite,
                    initialCameraPosition: CameraPosition(
                      target: LatLng(fields[0].startPosition.latitude, fields[0].startPosition.longitude),
                      zoom: fields[0].zoom,
                    ),
                    //onMapCreated: onMapCreated,
                    myLocationEnabled: true,
                    //zoomGesturesEnabled: true,
                    //zoomControlsEnabled: true,
                  ),

                  panelBuilder: (controller) => PanelWidget(
                    controller: controller,
                    panelController: panelController,
                  ),
                  borderRadius: BorderRadius.vertical(top: Radius.circular(18)),
                  onPanelSlide: (position) => setState(() {
                    final panelMaxScrollExtent = panelHeightOpen - panelHeightClosed;

                    fabHeight = position * panelMaxScrollExtent + fabHeightClosed;
                  }),
                ),*/
                Positioned(right: 15, bottom: fabHeight + 85, child: buildFAB(context)),
                Positioned(right: 15, bottom: fabHeight + 15, child: addField(context)),
                //Positioned(top: 52, child: buildTitle()),
                //
                fieldPannel(),
              /*Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: TextButton.icon(
                        icon: Icon(Icons.arrow_back),
                        label: Text(''),
                        onPressed: () {
                          setState(() {
                            fieldPosition--;
                            if(fieldPosition < 0 ) fieldPosition = fields.length-1;
                            CameraPosition camPos = CameraPosition(
                                bearing: fields[fieldPosition].bearing,
                                target:  LatLng(fields[fieldPosition].startPosition.latitude, fields[fieldPosition].startPosition.longitude),
                                tilt: fields[fieldPosition].tilt,
                                zoom: fields[fieldPosition].zoom);
                            _goToTheField(camPos);
                          });
                        },
                      ),
                  ),
                  Flexible(
                    child: TextButton.icon(
                      icon: Icon(Icons.map_outlined),
                      label: Text(fields[fieldPosition].plant),
                      onPressed: () {
                      },
                    ),
                  ),
                  Flexible(
                    child: TextButton.icon(
                      icon: Icon(Icons.arrow_forward),
                      label: Text(''),
                      onPressed: () {
                          fieldPosition++;
                          if(fieldPosition >= fields.length ) fieldPosition = 0;
                          CameraPosition camPos = CameraPosition(
                              bearing: fields[fieldPosition].bearing,
                              target:  LatLng(fields[fieldPosition].startPosition.latitude, fields[fieldPosition].startPosition.longitude),
                              tilt: fields[fieldPosition].tilt,
                              zoom: fields[fieldPosition].zoom);
                          _goToTheField(camPos);
                      },
                    ),
                  ),
                ],
              ),*/
              ],
            )
          );

        } else {
          return Container();
        }
        return Container(
              child: Column(
                children: [
                  SizedBox(width: 30),
                  CircularProgressIndicator(),
                  SizedBox(width: 10),
                  Text("Server: " + snapshot.connectionState.toString()),
                  Text("Wait for Field-data....")
                ],
            )

        );
      }
    );
  }

  Future<void> _goToTheField(CameraPosition camPos) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(camPos));
  }

  Widget buildFAB(BuildContext context) => FloatingActionButton(
    backgroundColor: Colors.white,
    child: Icon(
      Icons.photo_size_select_small_sharp,
      color: Theme.of(context).primaryColor,
    ),
    onPressed: () {
      overviewPos();
      },
  );

  Widget addField(BuildContext context) => FloatingActionButton(
    backgroundColor: Colors.white,
    child: Icon(
      Icons.add_location_alt_outlined,
      //Icons.gps_fixed,
      color: Theme.of(context).primaryColor,
    ),
    onPressed: () {
      showToast("not supported until now");
    },
  );

  void showToast(message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  Widget _child = Center(
    child: Text('Loading...'),
  );


  Widget fieldPannel() {
    return Column(
          children: <Widget>[
            //_child,
            AnimatedPositioned(
              bottom: _pinPillPosition,
              right: 0,
              left: 0,
              duration: Duration(milliseconds: 200),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin: EdgeInsets.all(20),
                  height: 70,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          blurRadius: 20,
                          offset: Offset.zero,
                          color: Colors.grey.withOpacity(0.5),
                        )
                      ]),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      _buildLeft(),
                      Headline(fieldName: fields[fieldPosition].plant,fieldNotice: fields[fieldPosition].notice),
                      _buildRight()
                    ],
                  ),
                ),
              ),
            )
          ],
        );
  }

  Widget _buildLeft() {
    return Container(
      margin: EdgeInsets.only(left: 10),
      width: 50,
      height: 50,
      child: ClipOval(
        child: TextButton.icon(
      icon: Icon(Icons.arrow_back),
      label: Text(''),
      onPressed: () {
          fieldPosition--;
          if(fieldPosition < 0 ) fieldPosition = fields.length-1;
          changePos();
          developer.log("Pos to the left " + keyHeadline.currentState.toString(), name: "MovePos");
          if (keyHeadline.currentState != null)
            keyHeadline.currentState!.setState(() {});
          },
        ),
      ),
    );
  }


  Widget _buildRight() {
    return Container(
      margin: EdgeInsets.only(left: 10),
      width: 50,
      height: 50,
      child: ClipOval(
        child: TextButton.icon(
          icon: Icon(Icons.arrow_forward),
          label: Text(''),
          onPressed: () {
            fieldPosition++;
            if(fieldPosition >= fields.length ) {
              fieldPosition = 0;
            }
              changePos();
              developer.log("Pos to the right "+ keyHeadline.currentState.toString(), name: "MovePos");
              if (keyHeadline.currentState != null)
                keyHeadline.currentState!.setState(() {});
              },
        ),
      ),
    );
  }


  Future<void> overviewPos() async {

    /*
    double longitudeMiddle = 0.0;
    double latitudeMiddle = 0.0;
    for(int x = 0; x < fields.length; x++){
      longitudeMiddle += fields[x].startPosition.longitude;
      latitudeMiddle += fields[x].startPosition.latitude;
    }
    longitudeMiddle /= fields.length;
    latitudeMiddle /= fields.length;*/
    List<LatLng> list = [];
    for(int x = 0; x < fields.length; x++){
      for(int y = 0; y < fields[x].corners.length; y++)
      list.add(LatLng(fields[x].corners[y].latitude, fields[x].corners[y].longitude));
    }

    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newLatLngBounds(boundsFromLatLngList(list), 1));
  }

  LatLngBounds boundsFromLatLngList(List<LatLng> list) {
    double x0=0.0, x1=0.0, y0=0.0, y1=0.0;
    for (LatLng latLng in list) {
      if (x0 == 0.0) {
        x0 = x1 = latLng.latitude;
        y0 = y1 = latLng.longitude;
      } else {
        if (latLng.latitude > x1) x1 = latLng.latitude;
        if (latLng.latitude < x0) x0 = latLng.latitude;
        if (latLng.longitude > y1) y1 = latLng.longitude;
        if (latLng.longitude < y0) y0 = latLng.longitude;
      }
    }
    return LatLngBounds(northeast: LatLng(x1, y1), southwest: LatLng(x0, y0));
  }



  void changePos(){
    CameraPosition camPos = CameraPosition(
        bearing: fields[fieldPosition].bearing,
        target:  LatLng(fields[fieldPosition].startPosition.latitude, fields[fieldPosition].startPosition.longitude),
        tilt: fields[fieldPosition].tilt,
        zoom: fields[fieldPosition].zoom);
    _goToTheField(camPos);
  }


}
  /*
  Widget buildTitle() => Container(
    child:Row(
      children: [
        SoilHeadline(text: "<<", space: 10,),
        SizedBox(width: 30),
        SoilHeadline(text: "          Oat          ",space: 70,),
        SizedBox(width: 30),
        SoilHeadline(text: ">>", space: 10,),
      ],
    )

  ),*/







//______________________________________________________________________________
//Classes:

class Headline extends StatefulWidget {
  final fieldName;
  final fieldNotice;
  const Headline({Key? key, required this.fieldName, required this.fieldNotice}) : super(key: key);

  @override
  _HeadlineState createState() => _HeadlineState();
}

class _HeadlineState extends State<Headline> {
  @override
  Widget build(BuildContext context) {
    return  Expanded(
      key: keyHeadline,
        child: Container(
          margin: EdgeInsets.only(left: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                widget.fieldName,
                style: CustomAppTheme().data.textTheme.headline6,
              ),
              Text(
                widget.fieldNotice,
                style: CustomAppTheme().data.textTheme.bodyText1,
              ),
            ],
          ),
        ),
      );
  }
}




class Field {
  List<GeoPoint> corners;
  String notice;
  String ownerID;
  String plant = "Weizen";
  GeoPoint startPosition;
  String timelineMap = "";
  double zoom;
  double tilt;
  double bearing;
  int createTime = DateTime
      .now()
      .millisecondsSinceEpoch;



  Field({
    required this.corners,
    required this.notice,
    required this.ownerID,
    required this.plant,
    required this.startPosition,
    required this.zoom,
    required this.tilt,
    required this.bearing,
    createTime,
    timelineMap,
  });


  void addCorners(GeoPoint value) {
    corners.add(value);
  }

  /*void addTimeLineItem(String name, bool finised, List<String> helper,
      double time) {
    TimelineField timeline = TimelineField(
        finished: finised, helper: helper, time: time);
    timelineMap.addAll({"sowing": timeline});
  }*/

  Field.fromJson(Map<String, dynamic> json)
      : this(
    corners: new List<GeoPoint>.from(json['corners']!),
    notice: json['notice']! as String,
    ownerID: json['ownerID']! as String,
    plant: json['plant']! as String,
    startPosition: json['startPosition']! as GeoPoint,
    timelineMap: json['timelineMap']! as String,
    zoom: json['zoom']! as double,
    tilt: json['tilt']! as double,
    createTime: json['createTime']! as int,
      bearing:  json['bearing']! as double,
  );

  Map<String, dynamic> toJson() => {
    'corners': corners,
    'notice': notice,
    'ownerID': ownerID,
    'plant': plant,
    'startPosition': startPosition,
    'timelineMap': timelineMap,
    'zoom': zoom,
    'createTime': createTime,
    'tilt': tilt,
    'bearing': bearing,
  };

}

class WorkToDo {
  String name = "";
  bool finished = false;
  List<String> helper = [];
  String chatID = "";
  double time = 1624707058252;

  WorkToDo({
    required this.chatID,
    required this.name,
    required this.finished,
    required this.helper,
    required this.time,
  });

  WorkToDo.fromJson(Map<String, Object?> json)
      : this(
    chatID: json['chatID']! as String,
    name: json['name']! as String,
    finished: json['finished']! as bool,
    helper: json['helper']! as List<String>,
    time: json['time']! as double,
  );

  Map<String, Object?> toJson() {
    return {
      'chatID': chatID,
      'name': name,
      'finished': finished,
      'helper': helper,
      'time': time,
    };
  }
}


class Helper {
  List<String> skills = [];
  bool freeWorker = true;
  String company = "";
  String name = "";
  double salary = 0;

  Helper({
    required this.skills,
    required this.freeWorker,
    required this.company,
    required this.name,
    required this.salary,
  });

  Helper.fromJson(Map<String, Object?> json)
      : this(
    skills: json['skills']! as List<String>,
    name: json['name']! as String,
    company: json['company']! as String,
    salary: json['salary']! as double,
    freeWorker: json['freeWorker']! as bool,
  );

  Map<String, Object?> toJson() {
    return {
      'skills': skills,
      'name': name,
      'company': company,
      'salary': salary,
      'freeWorker': freeWorker,
    };
  }
}

class Plants{
  String name = "";


}

