

import 'package:agrar_plant_manager/data/hardcoded_data.dart';
import 'package:agrar_plant_manager/main.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_search/dropdown_search.dart';

import 'dart:developer' as developer;

/*
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Insert Plant in Database',
      home: PlantPageState(),
    );
  }
}*/

// Define a custom Form widget.
class PlantPageState extends StatefulWidget {
  @override
  _PlantPage createState() => _PlantPage();
}

// Define a corresponding State class.
// This class holds data related to the Form.
class _PlantPage extends State<PlantPageState> {
  final PlantSpecific plantSpecific = PlantSpecific();
  final TextEditingController editingControllerName = TextEditingController();
  String headline = 'Plants - Admin for inserting';
  //kein Bock für alles andere nachträglcih einen ein zu bauen...

  @override
  void initState() {
    super.initState();

  }



  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(headline),
        backgroundColor: Colors.green,
      ),
      body: inputPage(context),
    );
  }



  Widget inputPage(BuildContext context) => Stack(
    children: [
      ListView(
        //key: PageStorageKey<String>('bliblablub'), bringt nix für die Labels... :(
        children: [
          const SizedBox(height: 5,),
          DropdownSearch(
            mode: Mode.MENU,
            showSelectedItem: true,
            items: allPlants,
            // searchBoxController: editingControllerName, geht auch nciht .... verzweiflung
            label: "Plant Name",
            onChanged: (state){
              plantSpecific.name = state.toString();
              setState(() {
                headline = state.toString();
              });
            },
          ),
          const SizedBox(height: 15,),
          Text("Plantintervall in Month"),
          const SizedBox(height: 5,),
          TextField(
            keyboardType: TextInputType.number,
            maxLength: 2,
            onChanged: (state){
              plantSpecific.plant_intervall_month = state.toString()as int;
            },),
          const SizedBox(height: 5,),
          Text("Harvestintervall per Year"),
          const SizedBox(height: 5,),
          TextField(
            keyboardType: TextInputType.number,
            maxLength: 2,
            onChanged: (state){
              plantSpecific.harvest_intervall_month = state.toString() as int;
            },),
          const SizedBox(height: 5,),
          Text("Keep on Field until sow again in Year "),
          const SizedBox(height: 5,),
          TextField(
            keyboardType: TextInputType.number,
            maxLength: 1,
            onChanged: (state){
              plantSpecific.keep_until_sow_again_years = state.toString() as int;
            },),
          const SizedBox(height: 5,),
          DropdownSearch(
            mode: Mode.MENU,
            showSelectedItem: true,
            items: month,
            label: "Sowing start time",
            onChanged: (state){
              plantSpecific.sowing_start = month.indexOf(state.toString()) ;
            },
          ),
          const SizedBox(height: 5,),
          DropdownSearch(
            mode: Mode.MENU,
            showSelectedItem: true,
            items: month,
            label: "Sowing end time",
            onChanged: (state){
              plantSpecific.sowing_end = month.indexOf(state.toString()) ;
            },
          ),
          const SizedBox(height: 5,),
          DropdownSearch(
            mode: Mode.MENU,
            showSelectedItem: true,
            items: month,
            label: "Harvest start time",
            onChanged: (state){
              plantSpecific.harvest_start = month.indexOf(state.toString()) ;
            },
          ),
          const SizedBox(height: 5,),
          DropdownSearch(
            mode: Mode.MENU,
            showSelectedItem: true,
            items: month,
            label: "Harvest end time",
            onChanged: (state){
              plantSpecific.harvest_end = month.indexOf(state.toString()) ;
            },
          ),
          const SizedBox(height: 15,),
          DropdownSearch(
            mode: Mode.MENU,
            showSelectedItem: true,
            items: categoriePlants,
            label: "categorie",
            onChanged: (state){
              plantSpecific.category = categoriePlants.indexOf(state.toString()) ;
            },
          ),
          const SizedBox(height: 15,),
          Text("pH-min"),
          TextField(
            keyboardType: TextInputType.number,
            maxLength: 3,
            onChanged: (state){
              if(state.toString().contains('.')&& state.toString().length > 2){
                plantSpecific.ph_min = state.toString() as double;
              }
              else if(state.toString().contains('.')){
                plantSpecific.ph_min = (state.toString() + "0") as double;
              }
              else {
                plantSpecific.ph_min = (state.toString() + ".0") as double;
              }
            },
          ),
          Text("pH-Max"),
          TextField(
            keyboardType: TextInputType.number,
            maxLength: 3,
            onChanged: (state){
              if(state.toString().contains('.')&& state.toString().length > 2){
                plantSpecific.ph_max = state.toString() as double;
              }
              else if(state.toString().contains('.')){
                plantSpecific.ph_max = (state.toString() + "0") as double;
              }
              else {
                plantSpecific.ph_max = (state.toString() + ".0") as double;
              }
            },
          ),
          const SizedBox(height: 15,),
          Text("Bodentyp Ziffer1:  ',' to seperate"),
          const SizedBox(height: 5,),
          Column(
            children:
            bodenTypeZiffer1.map((boden) {
              return Text(boden);
            }).toList(),
          ),
          TextField(
            onChanged: (state){
              plantSpecific.groundtype_1 = state as int;
            },),

          const SizedBox(height: 15,),
          Text("Bodentyp Ziffer2: ',' to seperate"),
          const SizedBox(height: 5,),
          Column(
            children:
            bodenTypeZiffer2.map((boden) {
              return Text(boden);
            }).toList(),
          ),
          TextField(
            maxLength: 1,
            onChanged: (state){
              plantSpecific.groundtype_2 = state as int;
            },
          ),
          const SizedBox(height: 5,),
          Text("seed costs €/ha"),
          TextField(
              keyboardType: TextInputType.number,
              onChanged: (state){
              plantSpecific.seed_cost = state as int;
            },
          ),
          const SizedBox(height: 5,),
          Text("seed demand dt/ha"),
          TextField(
            keyboardType: TextInputType.number,
            onChanged: (state){
              plantSpecific.seed_demand = state as int;
            },
          ),
          const SizedBox(height: 5,),
          Text("harvest costs €/ha"),
          TextField(
            keyboardType: TextInputType.number,
            onChanged: (state){
              plantSpecific.harvest_cost = state as int;
            },
          ),
          const SizedBox(height: 5,),
          Text("harvest yield dt/ha"),
          TextField(
            keyboardType: TextInputType.number,
            onChanged: (state){
              plantSpecific.harvest_yield = state as int;
            },
          ),

          const SizedBox(height: 25,),
          Text("Impact from previous crop - Quantity&Quality"),
          const SizedBox(height: 10,),
          Column(
            children:
              allPlants.map((plant) {
                return DropdownSearch(
                  mode: Mode.MENU,
                  showSelectedItem: true,
                  items: quality,
                  label: plant,
                  onChanged: (state){
                    plantSpecific.suitabilityAfter[plant] = quality.indexOf(state.toString());
                    //developer.log("Try to upload: " + plantSpecific.suitabilityAfter[plant].toString() + "  -  " + quality.indexOf(state.toString()).toString(), name: "Plant-QQ");
                  },
                );
              }).toList(),
          ),

          const SizedBox(height: 25,),
          Text("Impact - Special Attention"),
          Text("Seperate with ','"),
          const SizedBox(height: 10,),
          Column(
            children:
            allPlants.map((plant) {
              return Column(
                  children: [
                    const SizedBox(height: 5,),
                    Text(plant),
                    TextField(
                      onChanged: (state){
                        //Map<String,String> test = Map<String, String>();
                        //test.putIfAbsent(plant, () => state.toString());
                        //developer.log("Test: " +  test[plant].toString(), name: "Plant-Special");
                        plantSpecific.specialityAfter[plant] = state.toString();
                        //developer.log("Try to upload: " + plantSpecific.specialityAfter[plant].toString() + "  -  " + state.toString(), name: "Plant-Special");
                      },
                    )
                ],
              );
            }).toList(),
          ),

          ElevatedButton(
            style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20)),
            onPressed: () {
              //Überprüfen ob genügend Variablen eingefügt sind und dann auf Firebase uploaden
              developer.log("Try to upload: " + plantSpecific.name, name: "Plant-Uploader");
              _saveFBDataTest(context, plantSpecific);


              //Navigator.pop(context); // geht zurück
            },
            child: const Text('Upload'),
          ),
        ],
      )
    ],
  );


  Widget _saveFBDataTest(BuildContext context, PlantSpecific plantSpecific){
    String msg = "Upload..";
    Future<void> addField() async {
      CollectionReference fieldRef = FirebaseFirestore.instance.collection('plant');
      // Call the user's CollectionReference to add a new user

      fieldRef.add(plantSpecific.toJson())
          .then((value) => developer.log("Plant Added", name: "Plant-Uploader"))
          .catchError((error) => developer.log("Firebase Failed: $error", name: "Plant-Uploader")
      );

    }
    addField();
    return Center(
      child: Text(msg),
    );
  }

  //-----------------------------------------------------------------------------
//Hardcoded Data

  static const List<String> categoriePlants = [ // dafür sind arbeiten hinterlegt
    'Getreide',
    'Hackfrüchte',
    'Körnerleguminosen',
    'Hauptfutter- und Zwischenfruchtbau',
    'Feldgemüse'
  ];

  static const List<String> siknesReasonsPlants = [
    'Pilzl. Schaderreger besondere Fußkrankheiten',
    'Getreidezystenälchen',
    'Thyphula',
    'Mehltau',
    'Unverträglichkeit',
    'Fusariosen',
    'Kartoffelzystenählchen',
    'Pilzl. Schaderreger',
    'Rübenzystenählchen',
    'Kohlhernie',
    'Virosen',
    'pflansliche und tirische Schaderreger',
    'Unverträglichkeitsbeziehungen',
    'Fusariumwelke',
    'Blattfleckenkrankheit',
    'Kleekrebs',
    'Klappenschorf',
    'Brennfleckenkrankheit',
    'Fußkrankheiten',
    'Stängelälchen',
    'Lupinenwelke',
    'Kleeälchen',
    'Kleewürger',
    'Fusariumwelke u.a. pilzliche Erreger',
    'Rübenzystenälchen',
    'pilzliche Erreger (Kohlhernie)',
    'Nematoden',
    'Kartoffelzystenählchen',
  ];

  static const List<String> month = [
    'Januar',
    'Februar',
    'März',
    'April',
    'Mai',
    'Juni',
    'Juli',
    'August',
    'September',
    'Oktober',
    'November',
    'Dezember',
  ];

  static const List<String> quality = [
    '80-90%',
    '90-100%',
    '100-110%',
    '110-120%'
  ];

  static const List<String> bodenTypeZiffer1 = [
    '0 Mineralboden',
    '1 Humoser Boden (org. Substanz, 4 - 15%',
    '2 Anmoriger Boden (org. Substanz, 16 - 30%)',
    '3 Moor (org. Substanz > 30%; 2. Ziffer = 0',
  ];

  static const List<String> bodenTypeZiffer2 = [
    '1 Sand',
    '2 schwach lehmiger Sand',
    '3 stark lehmiger Sand',
    '4 sandiger Lehm',
    '5 Lößlehm, schluffiger Lehm',
    '6 toniger Lehm',
    '7 lehmiger Ton'
    '8 Ton'
  ];


  static const Map<String, String> specalInfo = {
    'a': 'Vorsicht bei starker Trockenheit',
    'b': 'Vegetationszeiten überschneiden sich (klimatische Randlage)',
    'c': 'Förderung bestimmter Krankheiten und Schädlinge, geringe Selbstverträglichkeit',
    'd': 'Förderung bestimmter Unkrautarten',
    'e': 'Vorfruchtwert wird schlecht ausgenutzt (Luxusfolge), mit Zweit- oder Zwischenfrucht evtl vertretbar',
    'f': 'Verminderung der Qualität der Nachfrucht möglich',
    'g': 'Lagergefahr der Nachfrucht',
    'h': 'Zwischnefrucht als Untersaat in Vorfrucht möglich bzw. günstig, insbesondere auf Standorten mit hoher Nährstoffauswaschung',
    'i': 'Zwischenfrucht als Soppelsaat oder Winterzwischenfrucht möglich bzw. günstig, insbesondere auf Standorten mit hoher Nährstoffauswaschung',
    'k': 'Auf leichten Böden günstig',
    'l': 'Organische Düngung zur Nachfrucht günstig',
    'm': 'Vorfrucht als Deckfrucht gut geeignet',
    'n': 'Vorfrucht als Deckfrucht bedingt geeignet',
    'o': 'Günstige Vorfrucht zur Reinsaat von Hauptfrüchten als Gründüngungs- u. Futterpflanzen',
    'p': 'Durchwuchsgefahr in der Nachfrucht (Saatgutvermehrung)',
    'r': 'Vor Saatfurche (intensive) Stoppelbearbeitung in Getreidefolgen möglich (Unkrautkur)',
  };



  static const List<String> allPlants = [
    'Luzerne',
    'Klee',
    'Leguminosengras',
    'Serradella',
    'Ackerbohne',
    'Erbse',
    'Linse',
    'Blaue Lupine',
    'Weiße Lupine',
    'Wicke',
    'Gelbe Lupine',
    'Sojabohne',
    'Gräser (ein- bis mehrjährig)',
    'Winter Weizen',
    'Sommer Weizen',
    'Durum',
    'Winter Roggen',
    'Triticale',
    'Winter Gerste',
    'Dinkel',
    'Sommer Gerste',
    'Hafer',
    'Silo-Mais',
    'Körner-Mais',
    'Zucker- und Futterrübe',
    'Frühkartoffel',
    'Mittelfrühe Kartoffel',
    'Späte Kartoffel',
    'Winter Raps',
    'Sonnenblume',
    'Lupine',
    'Weizen (Brauqualität)',
    'Winter Gerste (Futterqualität)',
    'Sommer Gerste (Futterqualität)',
    'Sommer Gerste (Brauqualität',
    'Silo- und Körnermais',
    'Futterrüben',
    'Zuckerrübe',
    'Frühkartoffel (Speisequalität)',
    'Mittelfrühe Kartoffel (Speisequalität)',
    'Späte Kartoffel (Verarbeitungsqualität)',
  ];

  static const Map<String,Map<String,String>> plantInformationText = {
    'Luzerne':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Klee':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Leguminosengras':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Serradella':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Ackerbohne':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Erbse':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Linse':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Blaue Lupine':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Weiße Lupine':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Wicke':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Gelbe Lupine':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Sojabohne':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Gräser (ein- bis mehrjährig)':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Winter Weizen':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Sommer Weizen':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Durum':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Winter Roggen':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Triticale':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Winter Gerste':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Dinkel':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Sommer Gerste':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Hafer':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Silo-Mais':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Körner-Mais':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Zucker- und Futterrübe':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Frühkartoffel':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Mittelfrühe Kartoffel':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Späte Kartoffel':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Winter Raps':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Sonnenblume':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Lupine':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Weizen (Brauqualität)':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Winter Gerste (Futterqualität)':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Sommer Gerste (Futterqualität)':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Sommer Gerste (Brauqualität':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Silo- und Körnermais':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Futterrüben':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Zuckerrübe':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Frühkartoffel (Speisequalität)':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Mittelfrühe Kartoffel (Speisequalität)':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
    'Späte Kartoffel (Verarbeitungsqualität)':{
      'Headline':'',
      'Boden':'',
      'Klima':'',
      'Aussaat':'',
      'Gemengeanbau':'',
      'Nutzung und Pflege':'',
      'Verwertungsformen bei Schnittnutzung':''},
  };





}







//------------------------------------------------------------------------------
//Classes

class PlantSpecific {
  int category = 1;
  String name = "";
  int sowing_start = 0;
  int sowing_end = 0;
  int harvest_start = 0;
  int harvest_end = 0;
  int plant_intervall_month = 12;
  int harvest_intervall_month = 1;
  Map<String, int> suitabilityAfter = Map<String, int>();
  //Map<String, String> specialityPrevius = Map<String, String>();
  Map<String, String> specialityAfter = Map<String, String>();
  int harvest_yield = 0;
  int harvest_cost = 0;
  int seed_demand = 0;
  int seed_cost = 0;

  int groundtype_1 = 0;
  int groundtype_2 = 0;

  double ph_min = 0.0;
  double ph_max = 0.0;

  int keep_until_sow_again_years = 0;
  //Map<String, String> suitabilityPrevius = Map<String, String>();


  PlantSpecific({
    category,
    name,
    sowing_start,
    sowing_end,
    harvest_start,
    harvest_end,
    specialityAfter,
    harvest_yield,
    //required this.specialityPrevius,
    suitabilityAfter,
    //required this.suitabilityPrevius,
    plant_intervall_month,
    harvest_intervall_month,
    keep_until_sow_again_years,
    harvest_cost,
    seed_demand,
    seed_cost,
    groundtype_1,
    groundtype_2,
    ph_min,
    ph_max,
  });


  PlantSpecific.fromJson(Map<String, dynamic> json)
  : this(
      category: json['category']! as int,
      name: json['name']! as String,
      sowing_start: json['sowing_start']! as int,
      sowing_end: json['sowing_end']! as int,
      harvest_start: json['harvest_start']! as int,
      harvest_end: json['harvest_end']! as int,
      specialityAfter: json['specialityAfter']! as Map<String,String>,
      //specialityPrevius: json['specialityPrevius']! as Map<String,String>,
      suitabilityAfter: json['suitabilityAfter']! as Map<String,int>,
      //suitabilityPrevius: json['suitabilityPrevius']! as Map<String,String>,
      plant_intervall_month: json['plant_intervall_month']! as int,
    harvest_intervall_month: json['harvest_intervall_month']! as int,
    keep_until_sow_again_years: json['keep_until_sow_again_years']! as int,
      harvest_yield: json['harvest_yield']! as int,
    harvest_cost: json['harvest_cost']! as int,
    seed_demand: json['seed_demand']! as int,
    seed_cost: json['seed_cost']! as int,
    groundtype_1: json['groundtype_1']! as int,
    groundtype_2: json['groundtype_2']! as int,
    ph_min: json['ph_min']! as double,
    ph_max: json['ph_max']! as double,
  );

  Map<String, dynamic> toJson() => {
    'category': category,
    'name': name,
    'sowing_start': sowing_start,
    'sowing_end': sowing_end,
    'harvest_start': harvest_start,
    'harvest_end': harvest_end,
    'specialityAfter': specialityAfter,
    //'specialityPrevius': specialityPrevius,
    'suitabilityAfter': suitabilityAfter,
    //'suitabilityPrevius': suitabilityPrevius,
    'plant_intervall_month': plant_intervall_month,
    'harvest_intervall_month': harvest_intervall_month,
    'keep_until_sow_again_years': keep_until_sow_again_years,
    'harvest_yield': harvest_yield,
    'harvest_cost': harvest_cost,
    'seed_demand': seed_demand,
    'seed_cost': seed_cost,
    'groundtype_1': groundtype_1,
    'groundtype_2': groundtype_2,
    'ph_min': ph_min,
    'ph_max': ph_max,
  };

}


/*class PlantCategorie{
  String name = "";

  PlantCategorie({
    required this.name,
  });

  PlantCategorie.fromJson(Map<String, dynamic> json)
      : this(
    name: json['name']! as String,
  );


  Map<String, dynamic> toJson() => {
    'name': name,
  };


}*/
